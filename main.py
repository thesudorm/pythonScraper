import requests
from bs4 import BeautifulSoup
import re

# Regex for cleaning up html tags from a string
TAG_RE = re.compile(r'<[^>]+>')

# CLASSES

class Comment(object):
    content = "Replace Content"
    author = "Replace Author"

    def __init__(self, newContent, newAuthor):
        self.content = newContent
        self.author = newAuthor

# FUNCTIONS #######################################

def getResponseObject(url):
    return requests.get(url, headers = {'User-agent': 'your bot 0.1'})

# Receives the raw html of a page and parses for each reddit comment on the page
def getComments(html_doc):

    # Get a list of the comment divs from the page
    soup = BeautifulSoup(html_doc, 'html.parser')
    comment_divs = soup.findAll("div", {"class":"Comment"})
    content_divs = soup.findAll("p", {"class": ["s570a4-10", "iEJDri"]})

    print(comment_divs)
    return content_divs

def cleanTags(raw_html):
    return TAG_RE.sub("", str(raw_html)) 
    #return toReturn.translate(None, string.punctuation)

## MAIN #######################################

url = "https://www.reddit.com/r/hacking/comments/9dau5j/an_albanian_virus/"

response = getResponseObject(url)
comments = getComments(response.content)
readable_comments = []

print("Number of comments: " + str(len(comments)))

# Clean the html tags off of each comment
for x in range(0, len(comments)):
    readable_comments.append(cleanTags(comments[x]).lower())

# Make an array for individual words

word_array = []

for x in range(0, len(readable_comments)):
    words = readable_comments[x].split()

    for word in words:
        word_array.append(word)

print(word_array)

# Write the data to a file

# Create a file object
# Write data to a javascript file in an array format
myFile = open("output.txt", "w")

for comment in readable_comments:
    myFile.write(comment)
    myFile.write("\n")

